﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMain
{
    public class Context : DbContext
    {
        public static MyContext ContextProp { get; set; }

        public static void Initialize()
        {
            ContextProp = new MyContext(CreateContextOptions());
        }

        public static DbContextOptions<MyContext> CreateContextOptions()
        {
            var connection = new SqliteConnection("Data Source=Employees.db");
            connection.Open();

            var option = new DbContextOptionsBuilder<MyContext>().UseSqlite(connection).Options;

            return option;
        }
    }
}
