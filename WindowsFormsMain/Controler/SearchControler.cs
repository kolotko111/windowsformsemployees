﻿using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMain.Controler
{
    class SearchControler
    {
        private MyContext db;

        public SearchControler(MyContext db)
        {
            this.db = db;
        }

        public IQueryable<Employee> SearchMethod(bool byName, bool byDepartment, bool byVoivodeship, string name, int departmentId, int VoivodeshipId)
        {
            IQueryable<Employee> query = db.EmployeeDb.AsQueryable();

            if (byName)
            query = query.Where(c => c.Name.Contains(name));

            if (byDepartment)
                query = query.Where(c => c.DepartmentId == departmentId);

            if (byVoivodeship)
                query = query.Where(c => c.VoivodeshipId == VoivodeshipId);

            return query;
        }
    }
}
