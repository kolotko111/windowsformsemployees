﻿using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMain.Controler
{
    public class DepartmentControler
    {
        private MyContext db;

        public DepartmentControler(MyContext db)
        {
            this.db = db;
        }

        public string AddDepartment(string name)
        {
            string message = null;
            // If name is empty dont create object 
            if (String.IsNullOrEmpty(name))
            {
                message = "The field containing the department name is empty";
                return message;
            }

            //Create new employee
            var dep = new Department()
            {
                Title = name,
            };

            //Add to data base
            db.DepartmentDb.Add(dep);
            db.SaveChanges();

            message = "The department has been successfully added";

            //return
            return message;
        }

        public string EditDepartment(string name, int id)
        {
            string message = null;
            // If name is empty dont create object 
            if (String.IsNullOrEmpty(name))
            {
                message = "The field containing the department name is empty";
                return message;
            }

            //to update
            var ToUpdate = db.DepartmentDb.SingleOrDefault(emp => emp.Id == id);

            //If we found element update title
            if (ToUpdate != null)
            {
                ToUpdate.Title = name;
                //save changes
                db.SaveChanges();
            }
            else
            {
                message = "The indicated element does not exist";
                return message;
            }

            message = "The department has been successfully edited";

            //return
            return message;
        }

        public string RemoveDepartment(int id)
        {
            string message = null;

            //to remove
            var ToRemove = db.DepartmentDb.SingleOrDefault(emp => emp.Id == id);

            if (ToRemove != null)
            {
                db.DepartmentDb.Remove(ToRemove);
                db.SaveChanges();
            }
            else
            {
                message = "Department was not found";
                return message;
            }
            message = "Department removed correctly";

            //return
            return message;
        }
    }
}
