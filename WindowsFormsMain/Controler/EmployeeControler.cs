﻿using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMain.Controler
{
    public class EmployeeControler
    {
        private MyContext db;

        public EmployeeControler(MyContext db)
        {
            this.db = db;
        }

        public string AddEmployees(string name, int departamentId, int voivodeshipId)
        {
            string message = null;
            // If name is empty dont create object 
            if (String.IsNullOrEmpty(name))
            {
                message = "The field containing the employee name is empty";
                return message;
            }

            // If name contain number dont create object
            if (name.Any(c => char.IsDigit(c)))
            {
                message = "The employee name field contains numbers, it is not allowed";
                return message;
            }

            //Check that department exist
            var IfTheDepartmentExists = db.DepartmentDb.SingleOrDefault(dep => dep.Id == departamentId);
            if (IfTheDepartmentExists == null)
            {
                message = "Department does not exist";
                return message;
            }

            //Check that department exist
            var IfTheVoivodeshipExists = db.VoivodeshipDb.SingleOrDefault(voi => voi.Id == voivodeshipId);
            if (IfTheVoivodeshipExists == null)
            {
                message = "Voivodeship does not exist";
                return message;
            }

            //Create new employee
            var emp = new Employee()
            {
                Name = name,
                DepartmentId = departamentId,
                VoivodeshipId = voivodeshipId,
            };

            //Add to data base
            db.EmployeeDb.Add(emp);
            db.SaveChanges();

            message = "The employee has been successfully added";

            //return
            return message;
        }

        public string EditEmployees(int id, string name, int departamentId, int voivodeshipId)
        {
            string message = null;
            // If name is empty dont create object 
            if (String.IsNullOrEmpty(name))
            {
                message = "The field containing the employee name is empty";
                return message;
            }

            // If name contain number dont create object
            if (name.Any(c => char.IsDigit(c)))
            {
                message = "The employee name field contains numbers, it is not allowed";
                return message;
            }

            //Check That Department Exist
            var CheckThatDepartmentExist = db.DepartmentDb.SingleOrDefault(dep => dep.Id == departamentId);
            if (CheckThatDepartmentExist == null)
            {
                message = "The indicated department does not exist";
                return message;
            }

            //Chech That Vodeship Exist
            var ChechThatVodeshipExist = db.VoivodeshipDb.SingleOrDefault(dep => dep.Id == voivodeshipId);
            if (ChechThatVodeshipExist == null)
            {
                message = "The indicated vodeship does not exist";
                return message;
            }


            //Check that Employee exist
            var ToUpdate = db.EmployeeDb.SingleOrDefault(emp => emp.Id == id);
            if (ToUpdate == null)
            {
                message = "Employee does not exist";
                return message;
            }
            else
            {
                ToUpdate.Name = name;
                ToUpdate.DepartmentId = departamentId;
                ToUpdate.VoivodeshipId = voivodeshipId;
                //save changes
                db.SaveChanges();
            }

            message = "The Employee has been successfully edited";

            //return
            return message;

        }

        public string RemoveEmployee(int id)
        {
            string message = null;

            //to remove
            var ToRemove = db.EmployeeDb.SingleOrDefault(emp => emp.Id == id);

            if (ToRemove != null)
            {
                db.EmployeeDb.Remove(ToRemove);
                db.SaveChanges();
            }
            else
            {
                message = "Employee was not found";
                return message;
            }
            message = "Employee removed correctly";

            //return
            return message;
        }
    }
}
