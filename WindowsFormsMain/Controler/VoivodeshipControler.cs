﻿using Models;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMain.Controler
{
    public class VoivodeshipControler
    {
        private MyContext db;

        public VoivodeshipControler(MyContext db)
        {
            this.db = db;
        }

        public string AddVoivodeship(string name)
        {
            string message = null;
            // If name is empty dont create object 
            if (String.IsNullOrEmpty(name))
            {
                message = "The field containing the voivodeship name is empty";
                return message;
            }

            //Create new employee
            var voi = new Voivodeship()
            {
                Title = name,
            };

            //Add to data base
            db.VoivodeshipDb.Add(voi);
            db.SaveChanges();

            message = "The voivodeship has been successfully added";

            //return
            return message;
        }

        public string EditVoivodeship(string name, int id)
        {
            string message = null;
            // If name is empty dont create object 
            if (String.IsNullOrEmpty(name))
            {
                message = "The field containing the voivodeship name is empty";
                return message;
            }

            //to update
            var ToUpdate = db.VoivodeshipDb.SingleOrDefault(emp => emp.Id == id);

            //If we found element update title
            if (ToUpdate != null)
            {
                ToUpdate.Title = name;
                //save changes
                db.SaveChanges();
            }
            else
            {
                message = "The indicated element does not exist";
                return message;
            }

            message = "The voivodeship has been successfully edited";

            //return
            return message;
        }

        public string RemoveVoivodeship(int id)
        {
            string message = null;

            //to remove
            var ToRemove = db.VoivodeshipDb.SingleOrDefault(emp => emp.Id == id);

            if (ToRemove != null)
            {
                db.VoivodeshipDb.Remove(ToRemove);
                db.SaveChanges();
            }
            else
            {
                message = "Voivodeship was not found";
                return message;
            }
            message = "Voivodeship removed correctly";

            //return
            return message;
        }
    }
}
