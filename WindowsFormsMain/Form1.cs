﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMain.UserControlFolder;

namespace WindowsFormsMain
{
    public partial class FormEmployee : MetroFramework.Forms.MetroForm
    {
        //Create data base instance
        private MyContext Db;   

        static FormEmployee _instance;
        public static FormEmployee Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FormEmployee();
                return _instance;
            }
        }

        public MetroFramework.Controls.MetroPanel MetroContainer
        {
            get { return metroPanel; }
            set { metroPanel = value; }
        }

        public MetroFramework.Controls.MetroLink MetroBack
        {
            get { return metroBack; }
            set { metroBack = value; }
        }

        public FormEmployee()
        {
            Context.Initialize();
            InitializeComponent();
            Db = new MyContext(Context.CreateContextOptions());
            Db.Database.Migrate();
        }

        private void FormEmployee_Load(object sender, EventArgs e)
        {
            metroBack.Visible = false;
            _instance = this;
            UcDashboardcs uc = new UcDashboardcs();
            uc.Dock = DockStyle.Fill;
            metroPanel.Controls.Add(uc);
        }

        private void metroBack_Click(object sender, EventArgs e)
        {
            metroPanel.Controls["UcDashboardcs"].BringToFront();
            metroBack.Visible = false;
        }
    }
}
