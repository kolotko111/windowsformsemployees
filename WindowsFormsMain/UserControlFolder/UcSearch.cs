﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using WindowsFormsMain.Controler;
using Models.Models;

namespace WindowsFormsMain.UserControlFolder
{
    public partial class UcSearch : MetroFramework.Controls.MetroUserControl
    {
        //Create data base instance
        MyContext Db;

        public UcSearch()
        {
            //initialize database
            Db = Context.ContextProp;
            InitializeComponent();
        }

        private void UcSearch_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        public void Initialize()
        {
            //Create list for table 
            var employees = from emp in Db.EmployeeDb
                            select new
                            {
                                emp.Id,
                                emp.Name,
                                Department = emp.Department.Title,
                                Voivodeship = emp.Voivodeship.Title
                            };
            metroGridSearchList.DataSource = employees.ToList();
            //Width for column
            metroGridSearchList.Columns[0].Width = 50;
            metroGridSearchList.Columns[1].Width = 200;
            metroGridSearchList.Columns[2].Width = 200;
            metroGridSearchList.Columns[3].Width = 200;

            //Data from table in DB to Department ComboBox
            metroComboBoxSearchDepartment.DataSource = Db.DepartmentDb.ToList();
            metroComboBoxSearchDepartment.DisplayMember = "Title";
            metroComboBoxSearchDepartment.ValueMember = "Id";

            //Data from table in DB to Voivodeship ComboBox
            metroComboBoxSearchVoivodeship.DataSource = Db.VoivodeshipDb.ToList();
            metroComboBoxSearchVoivodeship.DisplayMember = "Title";
            metroComboBoxSearchVoivodeship.ValueMember = "Id";
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //Create instance DepartmentControler
            SearchControler SearchInstance = new SearchControler(Db);

            //Create variable to search
            string searchName = metroTextSearchEmployeeName.Text;
            int departmentToSearch = Convert.ToInt32(metroComboBoxSearchDepartment.SelectedValue);
            int voivodeshipToSearch = Convert.ToInt32(metroComboBoxSearchVoivodeship.SelectedValue);
            bool enableSearchName = metroCheckBoxName.Checked;
            bool enableSearchDepartment = metroCheckBoxDepartment.Checked;
            bool enableSearchVoivodeship = metroCheckBoxVoivodeship.Checked;



            IQueryable<Employee> SortedCollection = SearchInstance.SearchMethod(enableSearchName, enableSearchDepartment, enableSearchVoivodeship, searchName, departmentToSearch, voivodeshipToSearch);

            var FinnalCollection = from item in SortedCollection
                                   select new
                                   {
                                       item.Id,
                                       item.Name,
                                       Department = item.Department.Title,
                                       Voivodeship = item.Voivodeship.Title
                                   };

            metroGridSearchList.DataSource = FinnalCollection.ToList();
            //Width for column
            metroGridSearchList.Columns[0].Width = 50;
            metroGridSearchList.Columns[1].Width = 200;
            metroGridSearchList.Columns[2].Width = 200;
            metroGridSearchList.Columns[3].Width = 200;
        }
    }
}
