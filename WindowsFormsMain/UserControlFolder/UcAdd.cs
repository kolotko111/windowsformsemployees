﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using WindowsFormsMain.Controler;

namespace WindowsFormsMain.UserControlFolder
{
    public partial class UcAdd : MetroFramework.Controls.MetroUserControl
    {
        //Create data base instance
        MyContext Db;

        public UcAdd()
        {
            //initialize database
            Db = Context.ContextProp;
            InitializeComponent();
        }

        private void UcAdd_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        public void Initialize()
        {
            //Create list for table 
            var employees = from emp in Db.EmployeeDb
                            select new
                            {
                                emp.Id,
                                emp.Name,
                                Department = emp.Department.Title,
                                Voivodeship = emp.Voivodeship.Title
                            };
            metroGridEmployeeList.DataSource = employees.ToList();
            //Width for column
            metroGridEmployeeList.Columns[0].Width = 50;
            metroGridEmployeeList.Columns[1].Width = 200;
            metroGridEmployeeList.Columns[2].Width = 200;
            metroGridEmployeeList.Columns[3].Width = 200;

            //Data from table in DB to Department ComboBox
            metroComboBoxDepartment.DataSource = Db.DepartmentDb.ToList();
            metroComboBoxDepartment.DisplayMember = "Title";
            metroComboBoxDepartment.ValueMember = "Id";

            //Data from table in DB to Voivodeship ComboBox
            metroComboBoxVoivodeship.DataSource = Db.VoivodeshipDb.ToList();
            metroComboBoxVoivodeship.DisplayMember = "Title";
            metroComboBoxVoivodeship.ValueMember = "Id";
        }

        private void metroButtonSave_Click(object sender, EventArgs e)
        {
            //Create instance EmployeeControler
            EmployeeControler EmployeeInstance = new EmployeeControler(Db);

            //Create variable to create employee
            var newNameForEmployee = metroTextEmployeeName.Text;
            var selectedDepartment = Convert.ToInt32(metroComboBoxDepartment.SelectedValue);
            var selectedVoivodeship = Convert.ToInt32(metroComboBoxVoivodeship.SelectedValue);

            //Call add method 
            var emp = EmployeeInstance.AddEmployees(newNameForEmployee, selectedDepartment, selectedVoivodeship);

            //If we create employee add to db, if we dont create show message
            if (emp == "The employee has been successfully added")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, emp, "Notification");
            }
        }
    }
}
