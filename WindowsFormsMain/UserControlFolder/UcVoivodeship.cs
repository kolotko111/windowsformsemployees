﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using WindowsFormsMain.Controler;

namespace WindowsFormsMain.UserControlFolder
{
    public partial class UcVoivodeship : MetroFramework.Controls.MetroUserControl
    {
        //Create data base instance
        MyContext Db;

        public UcVoivodeship()
        {
            //initialize database
            Db = Context.ContextProp;
            InitializeComponent();
        }

        private void UcVoivodeship_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        public void Initialize()
        {
            var Voivodeships = from emp in Db.VoivodeshipDb
                               select new
                               {
                                   emp.Id,
                                   emp.Title
                               };
            metroGridVoivodeshipList.DataSource = Voivodeships.ToList();
            //Width for column
            metroGridVoivodeshipList.Columns[0].Width = 50;
            metroGridVoivodeshipList.Columns[1].Width = 200;
        }

        private void metroButtonAdd_Click(object sender, EventArgs e)
        {
            //Create instance VoivodeshipControler
            VoivodeshipControler VoivodeshipInstance = new VoivodeshipControler(Db);

            //Create variable to create voivodeship
            string StringFromUcVoivodeshipAdd = metroTextVoivodeshipAdd.Text;

            //Call add method 
            string returnMessage = VoivodeshipInstance.AddVoivodeship(StringFromUcVoivodeshipAdd);

            //If we add Voivodeship, if we dont add show message
            if (returnMessage == "The voivodeship has been successfully added")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }

        private void metroGridVoivodeshipList_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridVoivodeshipList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridVoivodeshipList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //When chose data from data grid fill comboboxes and textblock
            metroTextVoivodeshipEdit.Text = SelectedRow.Cells["Title"].Value.ToString();
        }

        private void metroButtonSave_Click(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridVoivodeshipList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridVoivodeshipList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //Create instance VoivodeshipControler
            VoivodeshipControler VoivodeshipInstance = new VoivodeshipControler(Db);

            //Create variable to edit voivodeship
            int Id = Convert.ToInt32(SelectedRow.Cells["Id"].Value.ToString());
            string newNameForVoivodeship = metroTextVoivodeshipEdit.Text;

            //Call save method 
            string returnMessage = VoivodeshipInstance.EditVoivodeship(newNameForVoivodeship, Id);

            //If we edit Voivodeship, if we dont edit show message
            if (returnMessage == "The voivodeship has been successfully edited")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }

        private void metroButtonRemove_Click(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridVoivodeshipList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridVoivodeshipList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //Create instance VoivodeshipControler
            VoivodeshipControler VoivodeshipInstance = new VoivodeshipControler(Db);

            //Create variable to edit voivodeship
            var Id = Convert.ToInt32(SelectedRow.Cells["Id"].Value.ToString());

            //Call remove method 
            string returnMessage = VoivodeshipInstance.RemoveVoivodeship(Id);

            //If we remove voivodeship, if we dont remove show message
            if (returnMessage == "Voivodeship removed correctly")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }
    }
}
