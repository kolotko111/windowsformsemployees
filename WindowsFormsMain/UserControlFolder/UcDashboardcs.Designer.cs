﻿namespace WindowsFormsMain.UserControlFolder
{
    partial class UcDashboardcs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTileSearch = new MetroFramework.Controls.MetroTile();
            this.metroTileVoivodeship = new MetroFramework.Controls.MetroTile();
            this.metroTileDepartment = new MetroFramework.Controls.MetroTile();
            this.metroTileEdit = new MetroFramework.Controls.MetroTile();
            this.metroTileAdd = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // metroTileSearch
            // 
            this.metroTileSearch.ActiveControl = null;
            this.metroTileSearch.Location = new System.Drawing.Point(20, 195);
            this.metroTileSearch.Name = "metroTileSearch";
            this.metroTileSearch.Size = new System.Drawing.Size(720, 155);
            this.metroTileSearch.TabIndex = 14;
            this.metroTileSearch.Text = "Search in Data Base";
            this.metroTileSearch.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileSearch.UseSelectable = true;
            this.metroTileSearch.Click += new System.EventHandler(this.metroTileSearch_Click);
            // 
            // metroTileVoivodeship
            // 
            this.metroTileVoivodeship.ActiveControl = null;
            this.metroTileVoivodeship.Location = new System.Drawing.Point(575, 20);
            this.metroTileVoivodeship.Name = "metroTileVoivodeship";
            this.metroTileVoivodeship.Size = new System.Drawing.Size(165, 155);
            this.metroTileVoivodeship.TabIndex = 13;
            this.metroTileVoivodeship.Text = "Voivodeship";
            this.metroTileVoivodeship.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileVoivodeship.UseSelectable = true;
            this.metroTileVoivodeship.Click += new System.EventHandler(this.metroTileVoivodeship_Click);
            // 
            // metroTileDepartment
            // 
            this.metroTileDepartment.ActiveControl = null;
            this.metroTileDepartment.Location = new System.Drawing.Point(390, 20);
            this.metroTileDepartment.Name = "metroTileDepartment";
            this.metroTileDepartment.Size = new System.Drawing.Size(165, 155);
            this.metroTileDepartment.TabIndex = 12;
            this.metroTileDepartment.Text = "Department";
            this.metroTileDepartment.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileDepartment.UseSelectable = true;
            this.metroTileDepartment.Click += new System.EventHandler(this.metroTileDepartment_Click);
            // 
            // metroTileEdit
            // 
            this.metroTileEdit.ActiveControl = null;
            this.metroTileEdit.Location = new System.Drawing.Point(205, 20);
            this.metroTileEdit.Name = "metroTileEdit";
            this.metroTileEdit.Size = new System.Drawing.Size(165, 155);
            this.metroTileEdit.TabIndex = 11;
            this.metroTileEdit.Text = "Edit";
            this.metroTileEdit.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileEdit.UseSelectable = true;
            this.metroTileEdit.Click += new System.EventHandler(this.metroTileEdit_Click);
            // 
            // metroTileAdd
            // 
            this.metroTileAdd.ActiveControl = null;
            this.metroTileAdd.Location = new System.Drawing.Point(20, 20);
            this.metroTileAdd.Name = "metroTileAdd";
            this.metroTileAdd.Size = new System.Drawing.Size(165, 155);
            this.metroTileAdd.TabIndex = 10;
            this.metroTileAdd.Text = "Add";
            this.metroTileAdd.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileAdd.UseSelectable = true;
            this.metroTileAdd.Click += new System.EventHandler(this.metroTileAdd_Click);
            // 
            // UcDashboardcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroTileSearch);
            this.Controls.Add(this.metroTileVoivodeship);
            this.Controls.Add(this.metroTileDepartment);
            this.Controls.Add(this.metroTileEdit);
            this.Controls.Add(this.metroTileAdd);
            this.Name = "UcDashboardcs";
            this.Size = new System.Drawing.Size(760, 370);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile metroTileSearch;
        private MetroFramework.Controls.MetroTile metroTileVoivodeship;
        private MetroFramework.Controls.MetroTile metroTileDepartment;
        private MetroFramework.Controls.MetroTile metroTileEdit;
        private MetroFramework.Controls.MetroTile metroTileAdd;
    }
}
