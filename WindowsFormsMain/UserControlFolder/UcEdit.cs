﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using WindowsFormsMain.Controler;

namespace WindowsFormsMain.UserControlFolder
{
    public partial class UcEdit : MetroFramework.Controls.MetroUserControl
    {
        //Create data base instance
        MyContext Db;

        public UcEdit()
        {
            //initialize database
            Db = Context.ContextProp;
            InitializeComponent();
        }

        private void UcEdit_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        public void Initialize()
        {
            //Create list for table 
            var employees = from emp in Db.EmployeeDb
                            select new
                            {
                                emp.Id,
                                emp.Name,
                                Department = emp.Department.Title,
                                Voivodeship = emp.Voivodeship.Title
                            };
            metroGridEmployeeList.DataSource = employees.ToList();
            //Width for column
            metroGridEmployeeList.Columns[0].Width = 50;
            metroGridEmployeeList.Columns[1].Width = 200;
            metroGridEmployeeList.Columns[2].Width = 200;
            metroGridEmployeeList.Columns[3].Width = 200;

            //Data from table in DB to Department ComboBox
            metroComboBoxDepartment.DataSource = Db.DepartmentDb.ToList();
            metroComboBoxDepartment.DisplayMember = "Title";
            metroComboBoxDepartment.ValueMember = "Id";

            //Data from table in DB to Voivodeship ComboBox
            metroComboBoxVoivodeship.DataSource = Db.VoivodeshipDb.ToList();
            metroComboBoxVoivodeship.DisplayMember = "Title";
            metroComboBoxVoivodeship.ValueMember = "Id";
        }

        private void metroGridEmployeeList_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridEmployeeList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridEmployeeList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //When chose data from data grid fill comboboxes and textblock
            metroTextEmployeeName.Text = SelectedRow.Cells["Name"].Value.ToString();
            metroComboBoxDepartment.SelectedIndex = metroComboBoxDepartment.FindStringExact(SelectedRow.Cells["Department"].Value.ToString());
            metroComboBoxVoivodeship.SelectedIndex = metroComboBoxVoivodeship.FindStringExact(SelectedRow.Cells["Voivodeship"].Value.ToString());
        }

        private void metroButtonSave_Click(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridEmployeeList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridEmployeeList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //Create instance EmployeeControler
            EmployeeControler EmployeeInstance = new EmployeeControler(Db);

            //Create variable to edit
            var Id = Convert.ToInt32(SelectedRow.Cells["Id"].Value.ToString());
            var newNameForEmployee = metroTextEmployeeName.Text;
            var selectedDepartment = Convert.ToInt32(metroComboBoxDepartment.SelectedValue);
            var selectedVoivodeship = Convert.ToInt32(metroComboBoxVoivodeship.SelectedValue);

            //Call edit method 
            var returnMessage = EmployeeInstance.EditEmployees(Id, newNameForEmployee, selectedDepartment, selectedVoivodeship);

            //If we edit employee, if we dont edit show message
            if (returnMessage == "The Employee has been successfully edited")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }

        private void metroButtonRemove_Click(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridEmployeeList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridEmployeeList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //Create instance EmployeeControler
            EmployeeControler EmployeeInstance = new EmployeeControler(Db);

            //Create variable to deleat
            var Id = Convert.ToInt32(SelectedRow.Cells["Id"].Value.ToString());

            string returnMessage = EmployeeInstance.RemoveEmployee(Id);

            //If we remove Employee, if we dont remove show message
            if (returnMessage == "Employee removed correctly")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }
    }
}
