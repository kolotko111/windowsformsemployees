﻿namespace WindowsFormsMain.UserControlFolder
{
    partial class UcDepartment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroButtonRemove = new MetroFramework.Controls.MetroButton();
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            this.metroTextDepartmentEdit = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroButtonAdd = new MetroFramework.Controls.MetroButton();
            this.metroTextDepartmentAdd = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroGridDepartmentList = new MetroFramework.Controls.MetroGrid();
            this.metroPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridDepartmentList)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel2
            // 
            this.metroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel2.Controls.Add(this.metroButtonRemove);
            this.metroPanel2.Controls.Add(this.metroButtonSave);
            this.metroPanel2.Controls.Add(this.metroTextDepartmentEdit);
            this.metroPanel2.Controls.Add(this.metroLabel6);
            this.metroPanel2.Controls.Add(this.metroLabel2);
            this.metroPanel2.Controls.Add(this.metroLabel5);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(20, 195);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(350, 175);
            this.metroPanel2.TabIndex = 20;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroButtonRemove
            // 
            this.metroButtonRemove.Location = new System.Drawing.Point(218, 80);
            this.metroButtonRemove.Name = "metroButtonRemove";
            this.metroButtonRemove.Size = new System.Drawing.Size(75, 29);
            this.metroButtonRemove.TabIndex = 21;
            this.metroButtonRemove.Text = "Remove";
            this.metroButtonRemove.UseSelectable = true;
            this.metroButtonRemove.Click += new System.EventHandler(this.metroButtonRemove_Click);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(137, 80);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(75, 29);
            this.metroButtonSave.TabIndex = 20;
            this.metroButtonSave.Text = "Save";
            this.metroButtonSave.UseSelectable = true;
            this.metroButtonSave.Click += new System.EventHandler(this.metroButtonSave_Click);
            // 
            // metroTextDepartmentEdit
            // 
            // 
            // 
            // 
            this.metroTextDepartmentEdit.CustomButton.Image = null;
            this.metroTextDepartmentEdit.CustomButton.Location = new System.Drawing.Point(182, 1);
            this.metroTextDepartmentEdit.CustomButton.Name = "";
            this.metroTextDepartmentEdit.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextDepartmentEdit.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextDepartmentEdit.CustomButton.TabIndex = 1;
            this.metroTextDepartmentEdit.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextDepartmentEdit.CustomButton.UseSelectable = true;
            this.metroTextDepartmentEdit.CustomButton.Visible = false;
            this.metroTextDepartmentEdit.Lines = new string[0];
            this.metroTextDepartmentEdit.Location = new System.Drawing.Point(137, 45);
            this.metroTextDepartmentEdit.MaxLength = 32767;
            this.metroTextDepartmentEdit.Name = "metroTextDepartmentEdit";
            this.metroTextDepartmentEdit.PasswordChar = '\0';
            this.metroTextDepartmentEdit.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextDepartmentEdit.SelectedText = "";
            this.metroTextDepartmentEdit.SelectionLength = 0;
            this.metroTextDepartmentEdit.SelectionStart = 0;
            this.metroTextDepartmentEdit.ShortcutsEnabled = true;
            this.metroTextDepartmentEdit.Size = new System.Drawing.Size(210, 29);
            this.metroTextDepartmentEdit.TabIndex = 13;
            this.metroTextDepartmentEdit.UseSelectable = true;
            this.metroTextDepartmentEdit.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextDepartmentEdit.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(137, 15);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(31, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Edit";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(8, 50);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(123, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Department Name:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(84, 15);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(47, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "Mode:";
            // 
            // metroPanel1
            // 
            this.metroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.metroButtonAdd);
            this.metroPanel1.Controls.Add(this.metroTextDepartmentAdd);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroLabel8);
            this.metroPanel1.Controls.Add(this.metroLabel9);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(350, 175);
            this.metroPanel1.TabIndex = 19;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroButtonAdd
            // 
            this.metroButtonAdd.Location = new System.Drawing.Point(137, 81);
            this.metroButtonAdd.Name = "metroButtonAdd";
            this.metroButtonAdd.Size = new System.Drawing.Size(75, 29);
            this.metroButtonAdd.TabIndex = 13;
            this.metroButtonAdd.Text = "Add";
            this.metroButtonAdd.UseSelectable = true;
            this.metroButtonAdd.Click += new System.EventHandler(this.metroButtonAdd_Click);
            // 
            // metroTextDepartmentAdd
            // 
            // 
            // 
            // 
            this.metroTextDepartmentAdd.CustomButton.Image = null;
            this.metroTextDepartmentAdd.CustomButton.Location = new System.Drawing.Point(182, 1);
            this.metroTextDepartmentAdd.CustomButton.Name = "";
            this.metroTextDepartmentAdd.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextDepartmentAdd.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextDepartmentAdd.CustomButton.TabIndex = 1;
            this.metroTextDepartmentAdd.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextDepartmentAdd.CustomButton.UseSelectable = true;
            this.metroTextDepartmentAdd.CustomButton.Visible = false;
            this.metroTextDepartmentAdd.Lines = new string[0];
            this.metroTextDepartmentAdd.Location = new System.Drawing.Point(137, 46);
            this.metroTextDepartmentAdd.MaxLength = 32767;
            this.metroTextDepartmentAdd.Name = "metroTextDepartmentAdd";
            this.metroTextDepartmentAdd.PasswordChar = '\0';
            this.metroTextDepartmentAdd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextDepartmentAdd.SelectedText = "";
            this.metroTextDepartmentAdd.SelectionLength = 0;
            this.metroTextDepartmentAdd.SelectionStart = 0;
            this.metroTextDepartmentAdd.ShortcutsEnabled = true;
            this.metroTextDepartmentAdd.Size = new System.Drawing.Size(210, 29);
            this.metroTextDepartmentAdd.TabIndex = 19;
            this.metroTextDepartmentAdd.UseSelectable = true;
            this.metroTextDepartmentAdd.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextDepartmentAdd.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(137, 16);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(34, 19);
            this.metroLabel3.TabIndex = 18;
            this.metroLabel3.Text = "Add";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(8, 51);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(123, 19);
            this.metroLabel8.TabIndex = 16;
            this.metroLabel8.Text = "Department Name:";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(84, 16);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(47, 19);
            this.metroLabel9.TabIndex = 15;
            this.metroLabel9.Text = "Mode:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(390, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(99, 19);
            this.metroLabel1.TabIndex = 18;
            this.metroLabel1.Text = "All Department";
            // 
            // metroGridDepartmentList
            // 
            this.metroGridDepartmentList.AllowUserToResizeRows = false;
            this.metroGridDepartmentList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridDepartmentList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridDepartmentList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridDepartmentList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridDepartmentList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.metroGridDepartmentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridDepartmentList.DefaultCellStyle = dataGridViewCellStyle8;
            this.metroGridDepartmentList.EnableHeadersVisualStyles = false;
            this.metroGridDepartmentList.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridDepartmentList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridDepartmentList.Location = new System.Drawing.Point(390, 22);
            this.metroGridDepartmentList.Name = "metroGridDepartmentList";
            this.metroGridDepartmentList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridDepartmentList.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.metroGridDepartmentList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridDepartmentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridDepartmentList.Size = new System.Drawing.Size(350, 348);
            this.metroGridDepartmentList.TabIndex = 17;
            this.metroGridDepartmentList.SelectionChanged += new System.EventHandler(this.metroGridDepartmentList_SelectionChanged);
            // 
            // UcDepartment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroGridDepartmentList);
            this.Name = "UcDepartment";
            this.Size = new System.Drawing.Size(760, 370);
            this.Load += new System.EventHandler(this.UcDepartment_Load);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridDepartmentList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroButton metroButtonRemove;
        private MetroFramework.Controls.MetroButton metroButtonSave;
        private MetroFramework.Controls.MetroTextBox metroTextDepartmentEdit;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton metroButtonAdd;
        private MetroFramework.Controls.MetroTextBox metroTextDepartmentAdd;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroGrid metroGridDepartmentList;
    }
}
