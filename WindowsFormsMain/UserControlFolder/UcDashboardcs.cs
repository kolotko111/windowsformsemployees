﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsMain.UserControlFolder
{
    public partial class UcDashboardcs : MetroFramework.Controls.MetroUserControl
    {
        public UcDashboardcs()
        {
            InitializeComponent();
        }

        private void metroTileAdd_Click(object sender, EventArgs e)
        {
            if (!FormEmployee.Instance.MetroContainer.Controls.ContainsKey("UcAdd"))
            {
                UcAdd uc = new UcAdd();
                uc.Dock = DockStyle.Fill;
                FormEmployee.Instance.MetroContainer.Controls.Add(uc);
            }
            FormEmployee.Instance.MetroContainer.Controls["UcAdd"].BringToFront();
            FormEmployee.Instance.MetroBack.Visible = true;
        }

        private void metroTileEdit_Click(object sender, EventArgs e)
        {
            if (!FormEmployee.Instance.MetroContainer.Controls.ContainsKey("UcEdit"))
            {
                UcEdit uc = new UcEdit();
                uc.Dock = DockStyle.Fill;
                FormEmployee.Instance.MetroContainer.Controls.Add(uc);
            }
            FormEmployee.Instance.MetroContainer.Controls["UcEdit"].BringToFront();
            FormEmployee.Instance.MetroBack.Visible = true;
        }

        private void metroTileDepartment_Click(object sender, EventArgs e)
        {
            if (!FormEmployee.Instance.MetroContainer.Controls.ContainsKey("UcDepartment"))
            {
                UcDepartment uc = new UcDepartment();
                uc.Dock = DockStyle.Fill;
                FormEmployee.Instance.MetroContainer.Controls.Add(uc);
            }
            FormEmployee.Instance.MetroContainer.Controls["UcDepartment"].BringToFront();
            FormEmployee.Instance.MetroBack.Visible = true;
        }

        private void metroTileVoivodeship_Click(object sender, EventArgs e)
        {
            if (!FormEmployee.Instance.MetroContainer.Controls.ContainsKey("UcVoivodeship"))
            {
                UcVoivodeship uc = new UcVoivodeship();
                uc.Dock = DockStyle.Fill;
                FormEmployee.Instance.MetroContainer.Controls.Add(uc);
            }
            FormEmployee.Instance.MetroContainer.Controls["UcVoivodeship"].BringToFront();
            FormEmployee.Instance.MetroBack.Visible = true;
        }

        private void metroTileSearch_Click(object sender, EventArgs e)
        {
            if (!FormEmployee.Instance.MetroContainer.Controls.ContainsKey("UcSearch"))
            {
                UcSearch uc = new UcSearch();
                uc.Dock = DockStyle.Fill;
                FormEmployee.Instance.MetroContainer.Controls.Add(uc);
            }
            FormEmployee.Instance.MetroContainer.Controls["UcSearch"].BringToFront();
            FormEmployee.Instance.MetroBack.Visible = true;
        }
    }
}
