﻿namespace WindowsFormsMain.UserControlFolder
{
    partial class UcSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroCheckBoxVoivodeship = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBoxDepartment = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBoxName = new MetroFramework.Controls.MetroCheckBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxSearchVoivodeship = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxSearchDepartment = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroTextSearchEmployeeName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroGridSearchList = new MetroFramework.Controls.MetroGrid();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridSearchList)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.metroCheckBoxVoivodeship);
            this.metroPanel1.Controls.Add(this.metroCheckBoxDepartment);
            this.metroPanel1.Controls.Add(this.metroCheckBoxName);
            this.metroPanel1.Controls.Add(this.metroButton1);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.metroComboBoxSearchVoivodeship);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.metroComboBoxSearchDepartment);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.metroTextSearchEmployeeName);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 6);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(720, 96);
            this.metroPanel1.TabIndex = 19;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroCheckBoxVoivodeship
            // 
            this.metroCheckBoxVoivodeship.AutoSize = true;
            this.metroCheckBoxVoivodeship.Location = new System.Drawing.Point(602, 29);
            this.metroCheckBoxVoivodeship.Name = "metroCheckBoxVoivodeship";
            this.metroCheckBoxVoivodeship.Size = new System.Drawing.Size(95, 15);
            this.metroCheckBoxVoivodeship.TabIndex = 20;
            this.metroCheckBoxVoivodeship.Text = "Enable search";
            this.metroCheckBoxVoivodeship.UseSelectable = true;
            // 
            // metroCheckBoxDepartment
            // 
            this.metroCheckBoxDepartment.AutoSize = true;
            this.metroCheckBoxDepartment.Location = new System.Drawing.Point(270, 68);
            this.metroCheckBoxDepartment.Name = "metroCheckBoxDepartment";
            this.metroCheckBoxDepartment.Size = new System.Drawing.Size(95, 15);
            this.metroCheckBoxDepartment.TabIndex = 19;
            this.metroCheckBoxDepartment.Text = "Enable search";
            this.metroCheckBoxDepartment.UseSelectable = true;
            // 
            // metroCheckBoxName
            // 
            this.metroCheckBoxName.AutoSize = true;
            this.metroCheckBoxName.Location = new System.Drawing.Point(270, 29);
            this.metroCheckBoxName.Name = "metroCheckBoxName";
            this.metroCheckBoxName.Size = new System.Drawing.Size(95, 15);
            this.metroCheckBoxName.TabIndex = 18;
            this.metroCheckBoxName.Text = "Enable search";
            this.metroCheckBoxName.UseSelectable = true;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(622, 65);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 23);
            this.metroButton1.TabIndex = 17;
            this.metroButton1.Text = "Search";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(4, -1);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(124, 19);
            this.metroLabel2.TabIndex = 13;
            this.metroLabel2.Text = "Search in Data Base";
            // 
            // metroComboBoxSearchVoivodeship
            // 
            this.metroComboBoxSearchVoivodeship.FormattingEnabled = true;
            this.metroComboBoxSearchVoivodeship.ItemHeight = 23;
            this.metroComboBoxSearchVoivodeship.Location = new System.Drawing.Point(446, 20);
            this.metroComboBoxSearchVoivodeship.Name = "metroComboBoxSearchVoivodeship";
            this.metroComboBoxSearchVoivodeship.Size = new System.Drawing.Size(150, 29);
            this.metroComboBoxSearchVoivodeship.TabIndex = 17;
            this.metroComboBoxSearchVoivodeship.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(364, 25);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(82, 19);
            this.metroLabel5.TabIndex = 16;
            this.metroLabel5.Text = "Voivodeship:";
            // 
            // metroComboBoxSearchDepartment
            // 
            this.metroComboBoxSearchDepartment.FormattingEnabled = true;
            this.metroComboBoxSearchDepartment.ItemHeight = 23;
            this.metroComboBoxSearchDepartment.Location = new System.Drawing.Point(114, 59);
            this.metroComboBoxSearchDepartment.Name = "metroComboBoxSearchDepartment";
            this.metroComboBoxSearchDepartment.Size = new System.Drawing.Size(150, 29);
            this.metroComboBoxSearchDepartment.TabIndex = 15;
            this.metroComboBoxSearchDepartment.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(31, 64);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(83, 19);
            this.metroLabel4.TabIndex = 14;
            this.metroLabel4.Text = "Department:";
            // 
            // metroTextSearchEmployeeName
            // 
            // 
            // 
            // 
            this.metroTextSearchEmployeeName.CustomButton.Image = null;
            this.metroTextSearchEmployeeName.CustomButton.Location = new System.Drawing.Point(122, 1);
            this.metroTextSearchEmployeeName.CustomButton.Name = "";
            this.metroTextSearchEmployeeName.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextSearchEmployeeName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextSearchEmployeeName.CustomButton.TabIndex = 1;
            this.metroTextSearchEmployeeName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextSearchEmployeeName.CustomButton.UseSelectable = true;
            this.metroTextSearchEmployeeName.CustomButton.Visible = false;
            this.metroTextSearchEmployeeName.Lines = new string[0];
            this.metroTextSearchEmployeeName.Location = new System.Drawing.Point(114, 20);
            this.metroTextSearchEmployeeName.MaxLength = 32767;
            this.metroTextSearchEmployeeName.Name = "metroTextSearchEmployeeName";
            this.metroTextSearchEmployeeName.PasswordChar = '\0';
            this.metroTextSearchEmployeeName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextSearchEmployeeName.SelectedText = "";
            this.metroTextSearchEmployeeName.SelectionLength = 0;
            this.metroTextSearchEmployeeName.SelectionStart = 0;
            this.metroTextSearchEmployeeName.ShortcutsEnabled = true;
            this.metroTextSearchEmployeeName.Size = new System.Drawing.Size(150, 29);
            this.metroTextSearchEmployeeName.TabIndex = 9;
            this.metroTextSearchEmployeeName.UseSelectable = true;
            this.metroTextSearchEmployeeName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextSearchEmployeeName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(4, 25);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(110, 19);
            this.metroLabel3.TabIndex = 8;
            this.metroLabel3.Text = "Employee Name:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(20, 123);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(86, 19);
            this.metroLabel1.TabIndex = 18;
            this.metroLabel1.Text = "All Employee";
            // 
            // metroGridSearchList
            // 
            this.metroGridSearchList.AllowUserToResizeRows = false;
            this.metroGridSearchList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridSearchList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridSearchList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridSearchList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridSearchList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGridSearchList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridSearchList.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGridSearchList.EnableHeadersVisualStyles = false;
            this.metroGridSearchList.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridSearchList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridSearchList.Location = new System.Drawing.Point(20, 145);
            this.metroGridSearchList.Name = "metroGridSearchList";
            this.metroGridSearchList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridSearchList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGridSearchList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridSearchList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridSearchList.Size = new System.Drawing.Size(720, 219);
            this.metroGridSearchList.TabIndex = 17;
            // 
            // UcSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroGridSearchList);
            this.Name = "UcSearch";
            this.Size = new System.Drawing.Size(760, 370);
            this.Load += new System.EventHandler(this.UcSearch_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridSearchList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroCheckBox metroCheckBoxVoivodeship;
        private MetroFramework.Controls.MetroCheckBox metroCheckBoxDepartment;
        private MetroFramework.Controls.MetroCheckBox metroCheckBoxName;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox metroComboBoxSearchVoivodeship;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox metroComboBoxSearchDepartment;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox metroTextSearchEmployeeName;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroGrid metroGridSearchList;
    }
}
