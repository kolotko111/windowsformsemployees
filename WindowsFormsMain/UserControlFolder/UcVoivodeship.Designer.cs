﻿namespace WindowsFormsMain.UserControlFolder
{
    partial class UcVoivodeship
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroGridVoivodeshipList = new MetroFramework.Controls.MetroGrid();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroButtonRemove = new MetroFramework.Controls.MetroButton();
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            this.metroTextVoivodeshipEdit = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroButtonAdd = new MetroFramework.Controls.MetroButton();
            this.metroTextVoivodeshipAdd = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridVoivodeshipList)).BeginInit();
            this.metroPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(390, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(98, 19);
            this.metroLabel1.TabIndex = 26;
            this.metroLabel1.Text = "All Voivodeship";
            // 
            // metroGridVoivodeshipList
            // 
            this.metroGridVoivodeshipList.AllowUserToResizeRows = false;
            this.metroGridVoivodeshipList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridVoivodeshipList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridVoivodeshipList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridVoivodeshipList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridVoivodeshipList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGridVoivodeshipList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridVoivodeshipList.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGridVoivodeshipList.EnableHeadersVisualStyles = false;
            this.metroGridVoivodeshipList.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridVoivodeshipList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridVoivodeshipList.Location = new System.Drawing.Point(390, 22);
            this.metroGridVoivodeshipList.Name = "metroGridVoivodeshipList";
            this.metroGridVoivodeshipList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridVoivodeshipList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGridVoivodeshipList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridVoivodeshipList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridVoivodeshipList.Size = new System.Drawing.Size(350, 348);
            this.metroGridVoivodeshipList.TabIndex = 25;
            this.metroGridVoivodeshipList.SelectionChanged += new System.EventHandler(this.metroGridVoivodeshipList_SelectionChanged);
            // 
            // metroPanel2
            // 
            this.metroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel2.Controls.Add(this.metroButtonRemove);
            this.metroPanel2.Controls.Add(this.metroButtonSave);
            this.metroPanel2.Controls.Add(this.metroTextVoivodeshipEdit);
            this.metroPanel2.Controls.Add(this.metroLabel6);
            this.metroPanel2.Controls.Add(this.metroLabel2);
            this.metroPanel2.Controls.Add(this.metroLabel5);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(20, 195);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(350, 175);
            this.metroPanel2.TabIndex = 24;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroButtonRemove
            // 
            this.metroButtonRemove.Location = new System.Drawing.Point(218, 80);
            this.metroButtonRemove.Name = "metroButtonRemove";
            this.metroButtonRemove.Size = new System.Drawing.Size(75, 29);
            this.metroButtonRemove.TabIndex = 21;
            this.metroButtonRemove.Text = "Remove";
            this.metroButtonRemove.UseSelectable = true;
            this.metroButtonRemove.Click += new System.EventHandler(this.metroButtonRemove_Click);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(137, 80);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(75, 29);
            this.metroButtonSave.TabIndex = 20;
            this.metroButtonSave.Text = "Save";
            this.metroButtonSave.UseSelectable = true;
            this.metroButtonSave.Click += new System.EventHandler(this.metroButtonSave_Click);
            // 
            // metroTextVoivodeshipEdit
            // 
            // 
            // 
            // 
            this.metroTextVoivodeshipEdit.CustomButton.Image = null;
            this.metroTextVoivodeshipEdit.CustomButton.Location = new System.Drawing.Point(182, 1);
            this.metroTextVoivodeshipEdit.CustomButton.Name = "";
            this.metroTextVoivodeshipEdit.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextVoivodeshipEdit.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextVoivodeshipEdit.CustomButton.TabIndex = 1;
            this.metroTextVoivodeshipEdit.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextVoivodeshipEdit.CustomButton.UseSelectable = true;
            this.metroTextVoivodeshipEdit.CustomButton.Visible = false;
            this.metroTextVoivodeshipEdit.Lines = new string[0];
            this.metroTextVoivodeshipEdit.Location = new System.Drawing.Point(137, 45);
            this.metroTextVoivodeshipEdit.MaxLength = 32767;
            this.metroTextVoivodeshipEdit.Name = "metroTextVoivodeshipEdit";
            this.metroTextVoivodeshipEdit.PasswordChar = '\0';
            this.metroTextVoivodeshipEdit.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextVoivodeshipEdit.SelectedText = "";
            this.metroTextVoivodeshipEdit.SelectionLength = 0;
            this.metroTextVoivodeshipEdit.SelectionStart = 0;
            this.metroTextVoivodeshipEdit.ShortcutsEnabled = true;
            this.metroTextVoivodeshipEdit.Size = new System.Drawing.Size(210, 29);
            this.metroTextVoivodeshipEdit.TabIndex = 13;
            this.metroTextVoivodeshipEdit.UseSelectable = true;
            this.metroTextVoivodeshipEdit.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextVoivodeshipEdit.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(137, 15);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(31, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Edit";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(8, 50);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(122, 19);
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Voivodeship Name:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(84, 15);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(47, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "Mode:";
            // 
            // metroPanel1
            // 
            this.metroPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.metroButtonAdd);
            this.metroPanel1.Controls.Add(this.metroTextVoivodeshipAdd);
            this.metroPanel1.Controls.Add(this.metroLabel3);
            this.metroPanel1.Controls.Add(this.metroLabel8);
            this.metroPanel1.Controls.Add(this.metroLabel9);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(350, 175);
            this.metroPanel1.TabIndex = 23;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroButtonAdd
            // 
            this.metroButtonAdd.Location = new System.Drawing.Point(137, 81);
            this.metroButtonAdd.Name = "metroButtonAdd";
            this.metroButtonAdd.Size = new System.Drawing.Size(75, 29);
            this.metroButtonAdd.TabIndex = 13;
            this.metroButtonAdd.Text = "Add";
            this.metroButtonAdd.UseSelectable = true;
            this.metroButtonAdd.Click += new System.EventHandler(this.metroButtonAdd_Click);
            // 
            // metroTextVoivodeshipAdd
            // 
            // 
            // 
            // 
            this.metroTextVoivodeshipAdd.CustomButton.Image = null;
            this.metroTextVoivodeshipAdd.CustomButton.Location = new System.Drawing.Point(182, 1);
            this.metroTextVoivodeshipAdd.CustomButton.Name = "";
            this.metroTextVoivodeshipAdd.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metroTextVoivodeshipAdd.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextVoivodeshipAdd.CustomButton.TabIndex = 1;
            this.metroTextVoivodeshipAdd.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextVoivodeshipAdd.CustomButton.UseSelectable = true;
            this.metroTextVoivodeshipAdd.CustomButton.Visible = false;
            this.metroTextVoivodeshipAdd.Lines = new string[0];
            this.metroTextVoivodeshipAdd.Location = new System.Drawing.Point(137, 46);
            this.metroTextVoivodeshipAdd.MaxLength = 32767;
            this.metroTextVoivodeshipAdd.Name = "metroTextVoivodeshipAdd";
            this.metroTextVoivodeshipAdd.PasswordChar = '\0';
            this.metroTextVoivodeshipAdd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextVoivodeshipAdd.SelectedText = "";
            this.metroTextVoivodeshipAdd.SelectionLength = 0;
            this.metroTextVoivodeshipAdd.SelectionStart = 0;
            this.metroTextVoivodeshipAdd.ShortcutsEnabled = true;
            this.metroTextVoivodeshipAdd.Size = new System.Drawing.Size(210, 29);
            this.metroTextVoivodeshipAdd.TabIndex = 19;
            this.metroTextVoivodeshipAdd.UseSelectable = true;
            this.metroTextVoivodeshipAdd.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextVoivodeshipAdd.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(137, 16);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(34, 19);
            this.metroLabel3.TabIndex = 18;
            this.metroLabel3.Text = "Add";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(8, 51);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(122, 19);
            this.metroLabel8.TabIndex = 16;
            this.metroLabel8.Text = "Voivodeship Name:";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(84, 16);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(47, 19);
            this.metroLabel9.TabIndex = 15;
            this.metroLabel9.Text = "Mode:";
            // 
            // UcVoivodeship
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroGridVoivodeshipList);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Name = "UcVoivodeship";
            this.Size = new System.Drawing.Size(760, 370);
            this.Load += new System.EventHandler(this.UcVoivodeship_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroGridVoivodeshipList)).EndInit();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroGrid metroGridVoivodeshipList;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroButton metroButtonRemove;
        private MetroFramework.Controls.MetroButton metroButtonSave;
        private MetroFramework.Controls.MetroTextBox metroTextVoivodeshipEdit;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton metroButtonAdd;
        private MetroFramework.Controls.MetroTextBox metroTextVoivodeshipAdd;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
    }
}
