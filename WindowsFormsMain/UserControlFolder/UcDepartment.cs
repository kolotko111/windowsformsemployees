﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using WindowsFormsMain.Controler;

namespace WindowsFormsMain.UserControlFolder
{
    public partial class UcDepartment : MetroFramework.Controls.MetroUserControl
    {
        //Create data base instance
        MyContext Db;

        public UcDepartment()
        {
            //initialize database
            Db = Context.ContextProp;
            InitializeComponent();
        }

        private void UcDepartment_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        public void Initialize()
        {
            var Departments = from emp in Db.DepartmentDb
                              select new
                              {
                                  emp.Id,
                                  emp.Title
                              };
            metroGridDepartmentList.DataSource = Departments.ToList();
            //Width for column
            metroGridDepartmentList.Columns[0].Width = 50;
            metroGridDepartmentList.Columns[1].Width = 200;
        }

        private void metroButtonAdd_Click(object sender, EventArgs e)
        {
            //Create instance DepartmentControler
            DepartmentControler DepartmentInstance = new DepartmentControler(Db);

            //Create variable to create
            string newNameForDepartment = metroTextDepartmentAdd.Text;

            //Call add method 
            string returnMessage = DepartmentInstance.AddDepartment(newNameForDepartment);

            //If we add department to db, if we dont add show message
            if (returnMessage == "The department has been successfully added")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }

        private void metroGridDepartmentList_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridDepartmentList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridDepartmentList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //When chose data from data grid fill comboboxes and textblock
            metroTextDepartmentEdit.Text = SelectedRow.Cells["Title"].Value.ToString();
        }

        private void metroButtonSave_Click(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridDepartmentList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridDepartmentList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //Create instance DepartmentControler
            DepartmentControler DepartmentInstance = new DepartmentControler(Db);

            //Create variable to change department
            int Id = Convert.ToInt32(SelectedRow.Cells["Id"].Value.ToString());
            string newNameForDepartment = metroTextDepartmentEdit.Text;

            //Call edit method 
            string returnMessage = DepartmentInstance.EditDepartment(newNameForDepartment, Id);

            //If we edit Department, if we dont edit show message
            if (returnMessage == "The department has been successfully edited")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }

        private void metroButtonRemove_Click(object sender, EventArgs e)
        {
            DataGridViewRow SelectedRow = null;
            if (metroGridDepartmentList.SelectedRows.Count > 0)
            {
                SelectedRow = metroGridDepartmentList.SelectedRows[0];
            }

            if (SelectedRow == null)
            {
                return;
            }

            //Create instance DepartmentControler
            DepartmentControler DepartmentInstance = new DepartmentControler(Db);

            //Create variable to remove department
            var Id = Convert.ToInt32(SelectedRow.Cells["Id"].Value.ToString());

            //Call remove method 
            string returnMessage = DepartmentInstance.RemoveDepartment(Id);

            //If we remove Employee, if we dont remove show message
            if (returnMessage == "Department removed correctly")
            {
                Initialize();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, returnMessage, "Notification");
            }

            Initialize();
        }
    }
}
