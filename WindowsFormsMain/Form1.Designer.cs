﻿namespace WindowsFormsMain
{
    partial class FormEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroBack = new MetroFramework.Controls.MetroLink();
            this.metroPanel = new MetroFramework.Controls.MetroPanel();
            this.SuspendLayout();
            // 
            // metroBack
            // 
            this.metroBack.Image = global::WindowsFormsMain.Properties.Resources.BackIcon;
            this.metroBack.ImageSize = 27;
            this.metroBack.Location = new System.Drawing.Point(20, 21);
            this.metroBack.Name = "metroBack";
            this.metroBack.Size = new System.Drawing.Size(32, 32);
            this.metroBack.TabIndex = 5;
            this.metroBack.UseSelectable = true;
            this.metroBack.Click += new System.EventHandler(this.metroBack_Click);
            // 
            // metroPanel
            // 
            this.metroPanel.HorizontalScrollbarBarColor = true;
            this.metroPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel.HorizontalScrollbarSize = 10;
            this.metroPanel.Location = new System.Drawing.Point(20, 59);
            this.metroPanel.Name = "metroPanel";
            this.metroPanel.Size = new System.Drawing.Size(760, 370);
            this.metroPanel.TabIndex = 4;
            this.metroPanel.VerticalScrollbarBarColor = true;
            this.metroPanel.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel.VerticalScrollbarSize = 10;
            // 
            // FormEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.metroBack);
            this.Controls.Add(this.metroPanel);
            this.Name = "FormEmployee";
            this.Text = "     Employee";
            this.Load += new System.EventHandler(this.FormEmployee_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLink metroBack;
        private MetroFramework.Controls.MetroPanel metroPanel;
    }
}

