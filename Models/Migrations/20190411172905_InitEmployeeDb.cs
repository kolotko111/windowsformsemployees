﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Models.Migrations
{
    public partial class InitEmployeeDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DepartmentDb",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentDb", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VoivodeshipDb",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoivodeshipDb", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeDb",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    DepartmentId = table.Column<int>(nullable: false),
                    VoivodeshipId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeDb", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeDb_DepartmentDb_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "DepartmentDb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeDb_VoivodeshipDb_VoivodeshipId",
                        column: x => x.VoivodeshipId,
                        principalTable: "VoivodeshipDb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentDb_Id",
                table: "DepartmentDb",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeDb_DepartmentId",
                table: "EmployeeDb",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeDb_Id",
                table: "EmployeeDb",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeDb_VoivodeshipId",
                table: "EmployeeDb",
                column: "VoivodeshipId");

            migrationBuilder.CreateIndex(
                name: "IX_VoivodeshipDb_Id",
                table: "VoivodeshipDb",
                column: "Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeDb");

            migrationBuilder.DropTable(
                name: "DepartmentDb");

            migrationBuilder.DropTable(
                name: "VoivodeshipDb");
        }
    }
}
