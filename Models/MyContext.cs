﻿using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class MyContext : DbContext
    {
        public DbSet<Employee> EmployeeDb { get; set; }
        public DbSet<Department> DepartmentDb { get; set; }
        public DbSet<Voivodeship> VoivodeshipDb { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new EmployeConfig())
                   .ApplyConfiguration(new DepartmentConfig())
                   .ApplyConfiguration(new VoivodeshipConfig());

            base.OnModelCreating(builder);
        }

        public MyContext()
        {

        }

        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {

        }
    }
}
