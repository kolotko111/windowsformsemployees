﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class EmployeConfig : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder
                .Property(i => i.Id)
                .IsRequired();

            builder
                .HasIndex(p => p.Id)
                .IsUnique();

            builder
                .HasOne(e => e.Department)
                .WithMany(i => i.EmployeeCollectionInDepartment)
                .HasForeignKey(i => i.DepartmentId);

            builder
               .HasOne(e => e.Voivodeship)
               .WithMany(i => i.EmployeeCollectionInVoivodeship)
               .HasForeignKey(i => i.VoivodeshipId);

        }
    }
}
