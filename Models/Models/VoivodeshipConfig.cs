﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class VoivodeshipConfig : IEntityTypeConfiguration<Voivodeship>
    {
        public void Configure(EntityTypeBuilder<Voivodeship> builder)
        {
            builder
                .Property(i => i.Id)
                .IsRequired();

            builder
                .HasIndex(p => p.Id)
                .IsUnique();
        }
    }
}
