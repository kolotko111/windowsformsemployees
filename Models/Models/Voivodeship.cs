﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class Voivodeship
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public ICollection<Employee> EmployeeCollectionInVoivodeship { get; set; }
    }
}
