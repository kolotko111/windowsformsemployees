﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WindowsFormsMain.Controler;

namespace WindowsFormsEmployeesTests
{
    class EmployeeControlerTest
    {
        //empty name 
        [TestCase(null, 1, 1, "The field containing the employee name is empty")]
        //name contain numbers 
        [TestCase("Example Name1", 1, 1, "The employee name field contains numbers, it is not allowed")]
        //wrong departmentId
        [TestCase("Example Name", 2, 1, "Department does not exist")]
        //wrong voivodeship
        [TestCase("Example Name", 1, 2, "Voivodeship does not exist")]
        //good configuration
        [TestCase("Example Name", 1, 1, "The employee has been successfully added")]
        public void AddEmployeesTest(string name, int departamentId, int voivodeshipId, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            EmployeeControler EmployeeInstance = new EmployeeControler(context);
            DepartmentControler DepartmentInstance = new DepartmentControler(context);
            VoivodeshipControler VoivodeshiptInstance = new VoivodeshipControler(context);

            //Add department and voivodeship
            DepartmentInstance.AddDepartment("Name Before Changes");
            VoivodeshiptInstance.AddVoivodeship("Name Before Changes");
            #endregion

            //Act
            #region
            //call add method
            var returnMessage = EmployeeInstance.AddEmployees(name, departamentId, voivodeshipId);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }

        //empty name 
        [TestCase(1, null, 1, 1, "The field containing the employee name is empty")]
        //name contain numbers 
        [TestCase(1, "Example Name1", 1, 1, "The employee name field contains numbers, it is not allowed")]
        //wrong departmentId
        [TestCase(1, "Example Name", 5, 1, "The indicated department does not exist")]
        //wrong voivodeship
        [TestCase(1, "Example Name", 1, 5, "The indicated vodeship does not exist")]
        //wrong employee
        [TestCase(5, "Example Name", 1, 1, "Employee does not exist")]
        //good configuration
        [TestCase(1, "Example Name", 1, 1, "The Employee has been successfully edited")]
        public void EditEmployeesTest(int id, string name, int departamentId, int voivodeshipId, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            EmployeeControler EmployeeInstance = new EmployeeControler(context);
            DepartmentControler DepartmentInstance = new DepartmentControler(context);
            VoivodeshipControler VoivodeshiptInstance = new VoivodeshipControler(context);

            //Add department, voivodeship and employee
            DepartmentInstance.AddDepartment("Department1");
            DepartmentInstance.AddDepartment("Department2");
            VoivodeshiptInstance.AddVoivodeship("Vodeship1");
            VoivodeshiptInstance.AddVoivodeship("Vodeship2");
            EmployeeInstance.AddEmployees("Example name", departamentId, voivodeshipId);
            #endregion

            //Act
            #region
            //call edit method
            var returnMessage = EmployeeInstance.EditEmployees(id, name, departamentId, voivodeshipId);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }

        //wrong id
        [TestCase(5, "Employee was not found")]
        //good configuration
        [TestCase(1, "Employee removed correctly")]
        public void DeleatEmployeeTest(int id, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            EmployeeControler EmployeeInstance = new EmployeeControler(context);
            DepartmentControler DepartmentInstance = new DepartmentControler(context);
            VoivodeshipControler VoivodeshiptInstance = new VoivodeshipControler(context);

            //Create instance of common method
            DepartmentInstance.AddDepartment("Name Before Remove2");
            VoivodeshiptInstance.AddVoivodeship("Name Before Remove");
            EmployeeInstance.AddEmployees("Name", 1, 1);
            EmployeeInstance.AddEmployees("Name", 1, 1);
            #endregion

            //Act
            #region
            //call remove method
            var returnMessage = EmployeeInstance.RemoveEmployee(id);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }
    }
}
