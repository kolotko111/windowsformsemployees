﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WindowsFormsMain.Controler;

namespace WindowsFormsEmployeesTests
{
    class VoivodeshipControlerTest
    {
        //empty name
        [TestCase(null, "The field containing the voivodeship name is empty")]
        //good configuration
        [TestCase("Example Name", "The voivodeship has been successfully added")]
        public void AddVoivodeshipTest(string name, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            VoivodeshipControler VoivodeshipInstance = new VoivodeshipControler(context);
            #endregion

            //Act
            #region
            //call add method
            var returnMessage = VoivodeshipInstance.AddVoivodeship(name);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }

        //empty name
        [TestCase(null, 1, "The field containing the voivodeship name is empty")]
        //wrong id
        [TestCase("Example Name", 2, "The indicated element does not exist")]
        //good configuration
        [TestCase("Example Name", 1, "The voivodeship has been successfully edited")]
        public void EditVoivodeshipTest(string name, int id, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            VoivodeshipControler VoivodeshipInstance = new VoivodeshipControler(context);

            //Add voivodeship
            VoivodeshipInstance.AddVoivodeship("Name Before Changes");
            #endregion

            //Act
            #region
            //call edit method
            var returnMessage = VoivodeshipInstance.EditVoivodeship(name, id);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }

        // wrong id
        [TestCase(5, "Voivodeship was not found")]
        // good configuration
        [TestCase(1, "Voivodeship removed correctly")]
        public void DeleatVoivodeshipTest(int id, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            VoivodeshipControler VoivodeshipInstance = new VoivodeshipControler(context);

            //Add voivodeship
            VoivodeshipInstance.AddVoivodeship("Name Before Remove");
            VoivodeshipInstance.AddVoivodeship("Name Before Remove2");
            #endregion

            //Act
            #region
            //call remove method
            var returnMessage = VoivodeshipInstance.RemoveVoivodeship(id);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }
    }
}
