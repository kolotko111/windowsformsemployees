﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsFormsEmployeesTests
{
    class ConnectionFactory : IDisposable
    {
        #region IDisposable Support  
        private bool disposedValue = false; // To detect redundant calls  

        public MyContext CreateContextForInMemory()
        {
            var option = new DbContextOptionsBuilder<MyContext>().UseInMemoryDatabase(databaseName: "Test_Database").Options;

            var context = new MyContext(option);
            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            return context;
        }

        public MyContext CreateContextForSQLite()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var option = new DbContextOptionsBuilder<MyContext>().UseSqlite(connection).Options;

            var context = new MyContext(option);

            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            return context;
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
