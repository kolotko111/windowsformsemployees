﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using WindowsFormsMain.Controler;

namespace WindowsFormsEmployeesTests
{
    class DepartmentControlerTest
    {
        //empty name
        [TestCase(null, "The field containing the department name is empty")]
        //good configuration
        [TestCase("Example Name", "The department has been successfully added")]
        public void AddDepartmentTest(string name, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            DepartmentControler DepartmentInstance = new DepartmentControler(context);
            #endregion

            //Act
            #region
            //call add method
            var returnMessage = DepartmentInstance.AddDepartment(name);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }

        //empty name
        [TestCase(null, 1, "The field containing the department name is empty")]
        //wrong id
        [TestCase("Example Name", 2, "The indicated element does not exist")]
        //good configuration
        [TestCase("Example Name", 1, "The department has been successfully edited")]
        public void EditDepartmentTest(string name, int id, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            DepartmentControler DepartmentInstance = new DepartmentControler(context);

            //Add department
            DepartmentInstance.AddDepartment("Name before edit");
            #endregion

            //Act
            #region
            //call edit method
            var returnMessage = DepartmentInstance.EditDepartment(name, id);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }

        //wrong id
        [TestCase(5, "Department was not found")]
        //good configuration
        [TestCase(1, "Department removed correctly")]
        public void DeleatDepartmentTest(int id, string message)
        {
            //Arrange
            #region
            //Create db in memory and get instance
            var factory = new ConnectionFactory();
            var context = factory.CreateContextForSQLite();

            //Create instance for controler
            DepartmentControler DepartmentInstance = new DepartmentControler(context);

            //Add department
            DepartmentInstance.AddDepartment("Name Before Remove");
            DepartmentInstance.AddDepartment("Name Before Remove2");
            #endregion

            //Act
            #region
            //call remove method
            var returnMessage = DepartmentInstance.RemoveDepartment(id);
            #endregion

            // Assert:
            #region
            Assert.AreEqual(message, returnMessage);
            #endregion
        }
    }
}
